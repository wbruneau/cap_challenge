var displayedImage = document.querySelector('.displayed-img');
var thumbBar = document.querySelector('.thumb-bar');

btn = document.querySelector('button');
var overlay = document.querySelector('.overlay');

/* Looping through images */
for(i=1;i<=5;i++){
  var newImage = document.createElement('img');
  newImage.setAttribute('src', "img/pic"+i+".jpg");
  thumbBar.appendChild(newImage);
  newImage.addEventListener("click",changeImg);
}

function changeImg(e){
  displayedImage.setAttribute('src',e.target.getAttribute('src'))

}
/* Wiring up the Darken/Lighten button */
btn.addEventListener("click",darken);

function darken(){
  if(btn.getAttribute('class')==='dark'){
      btn.setAttribute('class','light');
      btn.textContent ='Lighten';
      overlay.style.backgroundColor = "rgba(0,0,0,0.5)";
  }else {
    btn.setAttribute('class','dark');
    btn.textContent ='Darken';
    overlay.style.backgroundColor = "rgba(0,0,0,0)";
  }

}
